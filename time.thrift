namespace php exp

struct Time {
  1: string text
}

service TimeService {
  void ping(),
  Time get_time()
}