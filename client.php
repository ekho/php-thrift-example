<?php
require_once 'lib/Thrift/ClassLoader/ThriftClassLoader.php';

$tcl = new \Thrift\ClassLoader\ThriftClassLoader();
$tcl->registerNamespace('Thrift', array('lib'));
$tcl->register();

use Thrift\Transport\TSocket;
use Thrift\Transport\TBufferedTransport;
use Thrift\Protocol\TBinaryProtocol;

require_once 'gen-php/exp/TimeService.php';
require_once 'gen-php/exp/Types.php';

$socket = new TSocket('localhost', 9090);
$transport = new TBufferedTransport($socket, 1024, 1024);
$protocol = new TBinaryProtocol($transport);
$client = new \exp\TimeServiceClient($protocol);

$transport->open();
var_dump($client->get_time());
